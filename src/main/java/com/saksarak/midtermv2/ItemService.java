/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.saksarak.midtermv2;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author LagunaZi
 */
public class ItemService {
    private static ArrayList<Item> itemList = new ArrayList<>();
    
    public static boolean addItem(Item item) {
        itemList.add(item);
        save();
        return true;
    }
    public static boolean delItem(Item item) {
        itemList.remove(item);
        save();
        return true;
    }
    public static boolean clearItem() {
        itemList.clear();
        save();
        return true;
    }
    public static boolean delItem(int index) {
        itemList.remove(index);
        save();
        return true;
    }
    public static ArrayList<Item> getItem() {
        return itemList;
    }
    public static Item getItem(int index) {
        return itemList.get(index);
    }
    public static boolean updateItem(int index, Item item) {
        itemList.set(index, item); 
        save();
        return true;
    }
    
    public static double getPriceResult(){
        double result = 0.00;
        for(Item item : itemList){
            result += item.getPrice()*item.getAmount();
        }
        return result;
    }
     public static double getAmountResult(){
        double amountresult = 0.00;
        for(Item item : itemList){
            amountresult += item.getAmount();
        }
        return amountresult;
    }
    
    public static void save() {
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {

            file = new File("Ton.dat");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(itemList);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ItemService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ItemService.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static void load() {
        File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {

            file = new File("Ton.dat");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            itemList = (ArrayList<Item>)ois.readObject();
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ItemService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ItemService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ItemService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
