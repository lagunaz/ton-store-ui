/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.saksarak.midtermv2;

import java.io.Serializable;

/**
 *
 * @author LagunaZi
 */
public class Item implements Serializable {
    private String id;
    private String name;
    private String brand;
    private Double price;
    private Double amount;

    public Item(String ID, String Name, String Brand, Double Price, Double Amount) {
        this.id = ID;
        this.name = Name;
        this.brand = Brand;
        this.price = Price;
        this.amount = Amount;
    } 

    public String getName() {
        return name;
    }

    public void setName(String Name) {
        this.name = Name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String Brand) {
        this.brand = Brand;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double Price) {
        this.price = Price;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double Amount) {
        this.amount = Amount;
    }

    public String getID() {
        return id;
    }

    public void setID(String ID) {
        this.id = ID;
    }

    @Override
    public String toString() {
        return "ID = " + id + " | Name = " + name + " | Brand = " + brand + " | Price = " + price + " | Amount = " + amount;
    }

   
    
}
